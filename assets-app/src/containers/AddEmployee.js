import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from  'react-redux';
import { FormattedMessage, injectIntl } from 'react-intl';
import { employeeAddRequested } from '../actions';
import { PrimaryButton, FormInput, Dialog } from '../components';
import './AddEmployee.css';

class AddEmployee extends Component {

  constructor(props){
    super(props);
    this.onAddEmployee = this.onAddEmployee.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange (e) {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  onAddEmployee(){
    this.props.onAddEmployee(this.state.employeeName);
  }

  render() {
    const { loading, message, intl } = this.props;
    return (
      <div className="container">
      <form>
        <label htmlFor="employeeName"><FormattedMessage id="AddEmployee.EmployeeName.Label"/>:</label>
        <FormInput name="employeeName" onChangeFunc={this.handleInputChange} />
        <PrimaryButton text={intl.formatMessage({id:"AddEmployee.PrimaryButton"})} onClickFunc={this.onAddEmployee} />
      </form>
      {loading && <p className="loading-message">LOADING...</p>}
      {message && <Dialog text={message} />}
      </div>
    );
  }
}

AddEmployee.propTypes = {
  loading: PropTypes.bool.isRequired,
  message: PropTypes.string,
};

const mapStateToProps = state => ({
  loading: state.employeeAdd.loading,
  message: state.employeeAdd.message
});

const mapDispatchToProps = dispatch => ({
  onAddEmployee: (employeeName) => dispatch(employeeAddRequested(employeeName))
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(AddEmployee));
