import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from  'react-redux';
import { injectIntl } from 'react-intl';
import { employeesGetRequested, assetsHistoryByEmployeeRequested } from '../actions';
import './ViewAssetsHistoryByEmployee.scss';
import EmployeesList from '../components/EmployeesList';
import AssetsHistory from '../components/AssetsHistory';

class ViewAssetsHistoryByEmployee extends Component {

  componentDidMount() {
    const { getEmployees } = this.props;
    getEmployees();
  }

  render() {
    const { loading, message, employees, assets, getAssetsHistoryByEmployee, selectedEmployee } = this.props;
    return (
      <div className="container">
        <EmployeesList employees={employees} selectedEmployee={selectedEmployee} onClickFunc={getAssetsHistoryByEmployee} />

        {assets && <AssetsHistory assets={assets}/>}
        
      {loading && <p className="loading-message">LOADING...</p>}
      {message && <p className="message-message">{message}</p>}
      </div>
    );
  }
}

ViewAssetsHistoryByEmployee.propTypes = {
  loading: PropTypes.bool.isRequired,
  message: PropTypes.string,
  getEmployees: PropTypes.func.isRequired,
  getAssetsHistoryByEmployee: PropTypes.func.isRequired,
  selectedEmployee: PropTypes.object
};

const mapStateToProps = state => ({
  loading: state.employeeAdd.loading,
  message: state.employeeAdd.message,
  employees: state.employeesGet.employees,
  assets: state.assetsHistoryByEmployee.assets,
  selectedEmployee: state.assetsHistoryByEmployee.selectedEmployee
});

const mapDispatchToProps = dispatch => ({
  getEmployees: () => dispatch(employeesGetRequested()),
  getAssetsHistoryByEmployee: (employee) => dispatch(assetsHistoryByEmployeeRequested(employee))
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(ViewAssetsHistoryByEmployee));
