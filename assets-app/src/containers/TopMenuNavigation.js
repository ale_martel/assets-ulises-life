import React, { Component } from 'react';
import { connect } from  'react-redux';
import PropTypes from 'prop-types';
import setLocale from '../actions/localeSet';
import { MenuNavigationItem, LanguageSelector } from '../components';
import './TopMenuNavigation.scss';

class TopMenuNavigation extends Component {
    render(){
        const { localeSet } = this.props;
        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav mr-auto">
                            <MenuNavigationItem route={'/home'} textKey={'Home.MenuLabel'} />
                            <MenuNavigationItem route={'/employees/add'} textKey={'AddEmployees.MenuLabel'} />
                            <MenuNavigationItem route={'/assets/add'} textKey={'AddAssets.MenuLabel'} />
                            <MenuNavigationItem route={'/assets/setEmployee'} textKey={'AssignAssetsToEmployee.MenuLabel'} />
                            <MenuNavigationItem route={'/assets/history'} textKey={'ViewEmployeeAssetsHistory.MenuLabel'} />
                        </ul>
                        <div className="form-inline my-2 my-lg-0">
                            <LanguageSelector languages={['es', 'en']} onClickFunc={localeSet} />
                        </div>
                    </div>
                </div>
            </nav>
        ) 
    }
}

TopMenuNavigation.protoTypes = {
    setLocale: PropTypes.func.isRequired,
    locale: PropTypes.string.isRequired
}

const mapStateToProps = state => ({
    locale: state.locale.loading,
  });

const mapDispatchToProps = dispatch => ({
    localeSet: lang => dispatch(setLocale(lang))
});

export default connect(mapStateToProps, mapDispatchToProps)(TopMenuNavigation);