import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from  'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { assetsGetRequested, assetSelectedSucceeded, setAssetToEmployeeRequested, employeesGetRequested } from '../actions';
import { EmployeesList, Dialog } from '../components';
import './SetAssetsToEmployee.scss';

class SetAssetsToEmployee extends Component {

  componentDidMount() {
    const { getAssets, getEmployees } = this.props;
    getEmployees();
    getAssets();
  }

  render() {
    const { loading, message, assets, selectedAssetId, selectAsset, employees, setAssetToEmployee } = this.props;
    return (
      <div className="container">
        <div className="card">
          <div className="card-header"><FormattedMessage id="SetAssetsToEmployee.AssetsList.Header"/></div>
          <div className="card-body">
          <ul className="list-group">
              {
              assets.map(asset => (
                  <li className={"list-group-item " + (asset.id === selectedAssetId ? 'active' : '')} key={asset.id}>
                    <label onClick={() => selectAsset(asset.id)}>{asset.name} (SN: {asset.serialNumber})</label>
                  </li>
              ))
              }
          </ul>
          </div>
        </div>
        
        {selectedAssetId && <EmployeesList employees={employees} onClickFunc={(employee) => setAssetToEmployee(selectedAssetId, employee.id)} />}
        
        {loading && <p className="loading-message">LOADING...</p>}
        {message && <Dialog text={message}/>}
      </div>
    );
  }
}

SetAssetsToEmployee.propTypes = {
  loading: PropTypes.bool.isRequired,
  message: PropTypes.string,
  getAssets: PropTypes.func.isRequired,
  selectAsset: PropTypes.func.isRequired,
  getEmployees: PropTypes.func.isRequired,
  employees: PropTypes.array,
  selectedAssetId: PropTypes.number
};

const mapStateToProps = state => ({
  loading: state.assetsGet.loading,
  message: state.setAssetToEmployee.message,
  employees: state.employeesGet.employees,
  assets: state.assetsGet.assets,
  selectedAssetId: state.assetsGet.selectedAssetId
});

const mapDispatchToProps = dispatch => ({
  getAssets: () => dispatch(assetsGetRequested()),
  selectAsset: (assetId) => dispatch(assetSelectedSucceeded(assetId)),
  getEmployees: () => dispatch(employeesGetRequested()),
  setAssetToEmployee: (selectedAssetId, employeeId) => dispatch(setAssetToEmployeeRequested(selectedAssetId, employeeId))
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(SetAssetsToEmployee));
