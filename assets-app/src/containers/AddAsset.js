import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from  'react-redux';
import { FormattedMessage, injectIntl } from 'react-intl';
import { assetAddRequested } from '../actions';
import { PrimaryButton, FormInput, Dialog } from '../components';
import './AddAsset.scss';

class AddAsset extends Component {
  constructor(props){
    super(props);
    this.onAddAsset = this.onAddAsset.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
  }

  handleInputChange (e) {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }
  handleCheckBoxChange (e) {
    const { name, checked } = e.target
    this.setState({
      [name]: checked
    })
  }

  onAddAsset(){
    this.props.onAddAsset(this.state.name, this.state.notes, this.state.isBlocked, this.state.serialNumber);
  }

  render() {
    const { loading, message, intl } = this.props
    return (
      <div className="container">
      <form onSubmit={this.handleSubmit}>
        <label htmlFor="name"><FormattedMessage id="AddAsset.Name.Label"/>: </label>
          <FormInput name="name" onChangeFunc={this.handleInputChange} />
        <label htmlFor="notes"><FormattedMessage id="AddAsset.Notes.Label"/>:</label>
          <textarea className="form-control" id="assetNotes" name="notes" onChange={this.handleInputChange} />
        <label htmlFor="blocked"><FormattedMessage id="AddAsset.Blocked.Label"/>: </label>
          <input type="checkbox" className="form-control" id="assetIsBlocked" name="isBlocked" onChange={this.handleCheckBoxChange} />
        <label htmlFor="notes"><FormattedMessage id="AddAsset.SerialNumber.Label"/>: </label>
          <FormInput name="serialNumber" onChangeFunc={this.handleInputChange} />
          <PrimaryButton text={intl.formatMessage({id:"AddAsset.PrimaryButton"})} onClickFunc={this.onAddAsset} />
      </form>
      {loading && <p className="loading-message">LOADING...</p>}
      {message && <Dialog text={message} />}
      </div>
    );
  }
}

AddAsset.propTypes = {
  loading: PropTypes.bool.isRequired,
  message: PropTypes.string
};

const mapStateToProps = state => ({
  loading: state.assetAdd.loading,
  message: state.assetAdd.message,
});

const mapDispatchToProps = dispatch => ({
  onAddAsset: (assetName, assetNotes, assetIsBlocked, assetSerialNumber) => dispatch(assetAddRequested(assetName, assetNotes, assetIsBlocked, assetSerialNumber))
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(AddAsset));
