import React, { Component } from 'react';
import imagesrc from '../resources/office-assets.png'

class Home extends Component {

  render() {
    return (
      <div className="container text-center mt-5">
        <img src={imagesrc} alt="office-assets" />
      </div>
    );
  }
}

export default Home;