var enLang = require('./en.json');
var esLang = require('./es.json');

export default {
    en: enLang,
    es: esLang
}