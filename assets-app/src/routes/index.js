import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import TopMenuNavigation from '../containers/TopMenuNavigation';
import AddEmployee from '../containers/AddEmployee';
import AddAsset from '../containers/AddAsset';
import ViewAssetsHistoryByEmployee from '../containers/ViewAssetsHistoryByEmployee';
import SetAssetsToEmployee from '../containers/SetAssetsToEmployee';
import Home from '../containers/Home';

export default () => (
    <BrowserRouter>
        <div>
            <Route path="/" component={TopMenuNavigation} />
            <Route path="/home" exact component={Home} />
            <Route path="/employees/add" component={AddEmployee} />
            <Route path="/assets/add" component={AddAsset} />
            <Route path="/assets/history" component={ViewAssetsHistoryByEmployee} />
            <Route path="/assets/setEmployee" component={SetAssetsToEmployee} />
        </div>
    </BrowserRouter>
)