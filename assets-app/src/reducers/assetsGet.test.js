import assetsGetReducer from './assetsGet';
import * as types from '../actions/actionTypes';

describe('assets get reducer', () => {
    it('should return the initial state', () => {
        expect(assetsGetReducer(undefined, {})).toEqual(
            { loading: false, message: null, assets: [], selectedAssetId: null }
        )
    });
  
    it('should handle ASSET_ADD_REQUESTED', () => {
        expect(assetsGetReducer([], {
            type: types.ASSETS_GET_REQUESTED,
            payload: {}
        })).toEqual(
            { loading: true, message: null, assets: [] }
        )
    });

    it('should handle ASSET_ADD_SUCCEEDED', () => {
        expect(assetsGetReducer([], {
            type: types.ASSETS_GET_SUCCEEDED,
            payload: [{
                id: 1,
                createDate: "2018-06-13T09:03:18.908Z",
                updateDate: "2018-06-13T09:03:18.908Z",
                name: "iPhone 5s",
                notes: null,
                blocked: false,
                serialNumber: "07fa0g9a70fq09u0bq90ba",
                employeeId: 2
            }]
        })).toEqual(
            { loading: false, message: null, assets: [{
                id: 1,
                createDate: "2018-06-13T09:03:18.908Z",
                updateDate: "2018-06-13T09:03:18.908Z",
                name: "iPhone 5s",
                notes: null,
                blocked: false,
                serialNumber: "07fa0g9a70fq09u0bq90ba",
                employeeId: 2
            }]}
        )
    });

    it('should handle ASSET_ADD_FAILED', () => {
        expect(assetsGetReducer([], {
            type: types.ASSETS_GET_FAILED,
            payload: {}
        })).toEqual(
            { loading: false, message: "An error occured getting the assets" }
        )
    });

    it('should handle ASSET_SELECTED', () => {
        expect(assetsGetReducer([], {
            type: types.ASSET_SELECTED,
            payload: 2
        })).toEqual(
            { selectedAssetId: 2 }
        )
    });
});