import employeesGetReducer from './employeesGet';
import * as types from '../actions/actionTypes';

describe('employees reducer', () => {
    it('should return the initial state', () => {
        expect(employeesGetReducer(undefined, {})).toEqual(
            { employees: [], loading: false, message: null }
        )
    });
  
    it('should handle EMPLOYEES_GET_REQUESTED', () => {
        expect(employeesGetReducer([], {
            type: types.EMPLOYEES_GET_REQUESTED,
            payload: {}
        })).toEqual(
            { employees: [], loading: true, message: null }
        )
    });

    it('should handle EMPLOYEES_GET_SUCCEEDED', () => {
        expect(employeesGetReducer([], {
            type: types.EMPLOYEES_GET_SUCCEEDED,
            payload: [
                {
                    "id": 1,
                    "createDate": "2018-06-13T08:50:00.592Z",
                    "updateDate": "2018-06-13T08:50:00.592Z",
                    "name": "Depot"
                },
                {
                    "id": 2,
                    "createDate": "2018-06-13T09:04:27.555Z",
                    "updateDate": "2018-06-13T09:04:27.555Z",
                    "name": "Ulises Santana"
                }
            ]
        })).toEqual(
            { employees: [
                {
                    "id": 1,
                    "createDate": "2018-06-13T08:50:00.592Z",
                    "updateDate": "2018-06-13T08:50:00.592Z",
                    "name": "Depot"
                },
                {
                    "id": 2,
                    "createDate": "2018-06-13T09:04:27.555Z",
                    "updateDate": "2018-06-13T09:04:27.555Z",
                    "name": "Ulises Santana"
                }
            ], loading: false, message: null }
        )
    });

    it('should handle EMPLOYEES_GET_FAILED', () => {
        expect(employeesGetReducer([], {
            type: types.EMPLOYEES_GET_FAILED,
            payload: {}
        })).toEqual(
            { loading: false, message: "An error occured getting employees" }
        )
    });
});