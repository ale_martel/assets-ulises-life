import * as types from '../actions/actionTypes';

const initState = {
  loading: false,
  message: null,
  assets: [],
  selectedAssetId: null
};

const assetsGetReducer = (state = initState, action) => {
  switch (action.type) {
    case types.ASSETS_GET_REQUESTED: {
        return { loading: true, message: null, assets: [] };
    }
    case types.ASSETS_GET_SUCCEEDED: {
        return {loading: false, message: null, assets: action.payload };
    }
    case types.ASSETS_GET_FAILED: {
        return { loading: false, message: "An error occured getting the assets" };
    }
    case types.ASSET_SELECTED: {
      return { ...state, selectedAssetId: action.payload };
  }
    default:
        return state;
  }
};

export default assetsGetReducer;