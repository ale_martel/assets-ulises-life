import * as types from '../actions/actionTypes';

const initState = {
  loading: false,
  message: null,
  assets: [],
  selectedEmployee: null
};

const assetsHistoryByEmployeeReducer = (state = initState, action) => {
  switch (action.type) {
    case types.ASSETS_HISTORY_BY_EMPLOYEE_REQUESTED: {
        return {...state, assets: [], loading: true, message: null, selectedEmployee: action.payload };
    }
    case types.ASSETS_HISTORY_BY_EMPLOYEE_SUCCEEDED: {
        return {...state, loading: false, message: null, assets: action.payload};
    }
    case types.ASSETS_HISTORY_BY_EMPLOYEE_FAILED: {
        return { ...state, loading: false, message: "An error occured getting assets history by employee" };
    }
    default:
        return state;
  }
};

export default assetsHistoryByEmployeeReducer;