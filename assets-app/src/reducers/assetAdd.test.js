import assetAddReducer from './assetAdd';
import * as types from '../actions/actionTypes';

describe('asset add reducer', () => {
    it('should return the initial state', () => {
        expect(assetAddReducer(undefined, {})).toEqual(
            { loading: false, message: null }
        )
    });
  
    it('should handle ASSET_ADD_REQUESTED', () => {
        expect(assetAddReducer([], {
            type: types.ASSET_ADD_REQUESTED,
            payload: {}
        })).toEqual(
            { loading: true, message: null }
        )
    });

    it('should handle ASSET_ADD_SUCCEEDED', () => {
        expect(assetAddReducer([], {
            type: types.ASSET_ADD_SUCCEEDED,
            payload: {}
        })).toEqual(
            { loading: false, message: "AddAsset.Success" }
        )
    });

    it('should handle ASSET_ADD_FAILED', () => {
        expect(assetAddReducer([], {
            type: types.ASSET_ADD_FAILED,
            payload: {}
        })).toEqual(
            { loading: false, message: "An error occured adding the asset" }
        )
    });
});