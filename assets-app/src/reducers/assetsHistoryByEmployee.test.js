import assetsHistoryByEmployeeReducer from './assetsHistoryByEmployee';
import * as types from '../actions/actionTypes';

describe('assets history by employee reducer', () => {
    it('should return the initial state', () => {
        expect(assetsHistoryByEmployeeReducer(undefined, {})).toEqual(
            { assets: [], loading: false, message: null, "selectedEmployee": null }
        )
    });
  
    it('should handle ASSETS_HISTORY_BY_EMPLOYEE_REQUESTED', () => {
        expect(assetsHistoryByEmployeeReducer([], {
            type: types.ASSETS_HISTORY_BY_EMPLOYEE_REQUESTED,
            payload: {id: 2, name: "employeeName"}
        })).toEqual(
            { assets: [], loading: true, message: null, "selectedEmployee": {id: 2, name: "employeeName"} }
        )
    });

    it('should handle ASSETS_HISTORY_BY_EMPLOYEE_SUCCEEDED', () => {
        expect(assetsHistoryByEmployeeReducer([], {
            type: types.ASSETS_HISTORY_BY_EMPLOYEE_SUCCEEDED,
            payload: [
                {
                    "id": 1,
                    "date": "2018-06-13T09:03:18.915Z",
                    "fromEmployee": "Depot",
                    "toEmployee": "Depot",
                    "asset": "iPhone 5s"
                },
                {
                    "id": 2,
                    "date": "2018-06-13T09:05:30.577Z",
                    "fromEmployee": "Depot",
                    "toEmployee": "Depot",
                    "asset": "Samsung"
                }
            ]
        })).toEqual(
            { assets: [
                {
                    "id": 1,
                    "date": "2018-06-13T09:03:18.915Z",
                    "fromEmployee": "Depot",
                    "toEmployee": "Depot",
                    "asset": "iPhone 5s"
                },
                {
                    "id": 2,
                    "date": "2018-06-13T09:05:30.577Z",
                    "fromEmployee": "Depot",
                    "toEmployee": "Depot",
                    "asset": "Samsung"
                }
            ], loading: false, message: null }
        )
    });

    it('should handle ASSETS_HISTORY_FAILED', () => {
        expect(assetsHistoryByEmployeeReducer([], {
            type: types.ASSETS_HISTORY_BY_EMPLOYEE_FAILED,
            payload: {}
        })).toEqual(
            { loading: false, message: "An error occured getting assets history by employee" }
        )
    });
});