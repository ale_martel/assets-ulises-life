import setAssetToEmployeeReducer from './setAssetToEmployee';
import * as types from '../actions/actionTypes';

describe('set asset to employee reducer', () => {
    it('should return the initial state', () => {
        expect(setAssetToEmployeeReducer(undefined, {})).toEqual(
            { loading: false, message: null }
        )
    });
  
    it('should handle SET_ASSET_TO_EMPLOYEE_REQUESTED', () => {
        expect(setAssetToEmployeeReducer([], {
            type: types.SET_ASSET_TO_EMPLOYEE_REQUESTED,
            payload: {}
        })).toEqual(
            { loading: true, message: null }
        )
    });

    it('should handle SET_ASSET_TO_EMPLOYEE_SUCCEEDED', () => {
        expect(setAssetToEmployeeReducer([], {
            type: types.SET_ASSET_TO_EMPLOYEE_SUCCEEDED,
            payload: {}
        })).toEqual(
            { loading: false, message: "AssignAssetsToEmployee.Success.Message" }
        )
    });

    it('should handle ASSET_ADD_FAILED', () => {
        expect(setAssetToEmployeeReducer([], {
            type: types.SET_ASSET_TO_EMPLOYEE_FAILED,
            payload: {}
        })).toEqual(
            { loading: false, message: "An error occured seting asset to an employee" }
        )
    });
});