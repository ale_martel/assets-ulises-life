import * as types from '../actions/actionTypes';

const initState = {
  loading: false,
  message: null,
};

const setAssetToEmployeeReducer = (state = initState, action) => {
  switch (action.type) {
    case types.SET_ASSET_TO_EMPLOYEE_REQUESTED: {
        return { loading: true, message: null };
    }
    case types.SET_ASSET_TO_EMPLOYEE_SUCCEEDED: {
        return {loading: false, message: "AssignAssetsToEmployee.Success.Message" };
    }
    case types.SET_ASSET_TO_EMPLOYEE_FAILED: {
        return { loading: false, message: "An error occured seting asset to an employee" };
    }
    default:
        return state;
  }
};

export default setAssetToEmployeeReducer;