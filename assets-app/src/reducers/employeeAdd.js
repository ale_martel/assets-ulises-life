import * as types from '../actions/actionTypes';

const initState = {
  loading: false,
  message: null,
};

const employeeAddReducer = (state = initState, action) => {
  switch (action.type) {
    case types.EMPLOYEE_ADD_REQUESTED: {
        return { loading: true, message: null };
    }
    case types.EMPLOYEE_ADD_SUCCEEDED: {
        return {loading: false, message: "AddEmployee.Success" };
    }
    case types.EMPLOYEE_ADD_FAILED: {
        return { loading: false, message: "An error occured adding the employee" };
    }
    default:
        return state;
  }
};

export default employeeAddReducer;