import localeSetReducer from './locale';
import * as types from '../actions/actionTypes';

describe('locale reducer', () => {
  it('should return the initial state', () => {
      expect(localeSetReducer(undefined, {})).toEqual(
          { lang: 'en' }
      )
  });

  it('should handle LOCALE_SET', () => {
      expect(localeSetReducer([], {
          type: types.LOCALE_SET,
          lang: 'es'
      })).toEqual(
          { lang: 'es' }
      )
  });
});