import { combineReducers } from 'redux';
import employeeAdd from './employeeAdd';
import assetAdd from './assetAdd';
import locale from './locale';
import authentication from './authentication';
import employeesGet from './employeesGet';
import assetsHistoryByEmployee from './assetsHistoryByEmployee';
import assetsGet from './assetsGet';
import setAssetToEmployee from './setAssetToEmployee';

const reducer = combineReducers({
  locale,
  employeeAdd,
  assetAdd,
  authentication,
  employeesGet,
  assetsHistoryByEmployee,
  assetsGet,
  setAssetToEmployee
});

export default reducer;