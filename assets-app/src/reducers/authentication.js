import * as types from '../actions/actionTypes';

const initState = {
  token: '',
  loading: false,
  message: null,
};

const authenticationReducer = (state = initState, action) => {
  
  switch (action.type) {
    case types.AUTHENTICATION_REQUESTED: {
        return { loading: true, message: null };
    }
    case types.AUTHENTICATION_SUCCEEDED: {
        localStorage.setItem('token', action.payload.token);
        return { token: action.payload.token, loading: false, message: null };
    }
    case types.AUTHENTICATION_FAILED: {
        localStorage.setItem('token', '');
        return { token:'', loading: false, message: "An error occured authenticating" };
    }
    default:
        return state;
  }
};

export default authenticationReducer;