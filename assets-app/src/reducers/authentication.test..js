import authenticationReducer from './authentication';
import * as types from '../actions/actionTypes';

describe('authentication reducer', () => {
    it('should return the initial state', () => {
        expect(authenticationReducer(undefined, {})).toEqual(
            { token:'', loading: false, message: null }
        )
    });
  
    it('should handle AUTHORIZATION_REQUESTED', () => {
        expect(authenticationReducer([], {
            type: types.AUTHORIZATION_REQUESTED,
            payload: {}
        })).toEqual(
            { loading: true, message: null }
        )
    });

    it('should handle AUTHORIZATION_SUCCEEDED', () => {
        expect(authenticationReducer([], {
            type: types.AUTHORIZATION_SUCCEEDED,
            payload: { token: "aToken" }
        })).toEqual(
            { token:"aToken", loading: false, message: null }
        )
    });

    it('should handle AUTHORIZATION_FAILED', () => {
        expect(authenticationReducer([], {
            type: types.AUTHORIZATION_FAILED,
            payload: {}
        })).toEqual(
            { loading: false, message: "An error occured authenticating" }
        )
    });
});