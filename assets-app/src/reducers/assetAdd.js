import * as types from '../actions/actionTypes';

const initState = {
  loading: false,
  message: null,
};

const assetAddReducer = (state = initState, action) => {
  switch (action.type) {
    case types.ASSET_ADD_REQUESTED: {
        return { loading: true, message: null };
    }
    case types.ASSET_ADD_SUCCEEDED: {
        return {loading: false, message: "AddAsset.Success" };
    }
    case types.ASSET_ADD_FAILED: {
        return { loading: false, message: "An error occured adding the asset" };
    }
    default:
        return state;
  }
};

export default assetAddReducer;