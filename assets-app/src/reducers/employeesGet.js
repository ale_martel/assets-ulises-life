import * as types from '../actions/actionTypes';

const initState = {
  loading: false,
  message: null,
  employees: []
};

const employeesGetReducer = (state = initState, action) => {
  switch (action.type) {
    case types.EMPLOYEES_GET_REQUESTED: {
        return {...state, employees: [], loading: true, message: null };
    }
    case types.EMPLOYEES_GET_SUCCEEDED: {
        return {...state, loading: false, message: null, employees: action.payload};
    }
    case types.EMPLOYEES_GET_FAILED: {
        return { ...state, loading: false, message: "An error occured getting employees" };
    }
    default:
        return state;
  }
};

export default employeesGetReducer;