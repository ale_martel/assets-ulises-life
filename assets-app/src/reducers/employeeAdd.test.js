import employeeAddReducer from './employeeAdd';
import * as types from '../actions/actionTypes';

describe('employee add reducer', () => {
    it('should return the initial state', () => {
        expect(employeeAddReducer(undefined, {})).toEqual(
            { loading: false, message: null }
        )
    });
  
    it('should handle EMPLOYEE_ADD_REQUESTED', () => {
        expect(employeeAddReducer([], {
            type: types.EMPLOYEE_ADD_REQUESTED,
            payload: {}
        })).toEqual(
            { loading: true, message: null }
        )
    });

    it('should handle EMPLOYEE_ADD_SUCCEEDED', () => {
        expect(employeeAddReducer([], {
            type: types.EMPLOYEE_ADD_SUCCEEDED,
            payload: {}
        })).toEqual(
            { loading: false, message: "AddEmployee.Success" }
        )
    });

    it('should handle EMPLOYEE_ADD_FAILED', () => {
        expect(employeeAddReducer([], {
            type: types.EMPLOYEE_ADD_FAILED,
            payload: {}
        })).toEqual(
            { loading: false, message: "An error occured adding the employee" }
        )
    });
});