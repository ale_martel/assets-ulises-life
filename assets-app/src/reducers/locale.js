import { LOCALE_SET } from '../actions/actionTypes';

const initState = {
  lang: "en"
};

const localeReducer = (state = initState, action = {}) => {
  switch (action.type) {
    case LOCALE_SET:
      localStorage.setItem('appLang', action.lang);
      return { lang: action.lang };
    default:
        return state;
  }
};

export default localeReducer;