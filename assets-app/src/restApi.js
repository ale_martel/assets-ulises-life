var environment = require('./environment.json');

export const authenticationRequestedApiCall = () => (
    fetch(`${environment.ApiUrl}/users/login`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: environment.LoginEmail,
            password: environment.LoginPassword
        })
    }).then(response => response.json())
);

export const employeeAddRequestedApiCall = employeeName => (
    fetch(`${environment.ApiUrl}/employees/`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.token}`
        },
        body: JSON.stringify({
            name: employeeName
        })
    }).then(response => response.json())
);

export const assetAddRequestedApiCall = asset => (
    fetch(`${environment.ApiUrl}/assets/`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.token}`
        },
        body: JSON.stringify({
            name: asset.name,
            notes: asset.notes,
            blocked: asset.isBlocked,
            serialNumber: asset.serialNumber
        })
    }).then(response => response.json())
);

export const employeesRequestedApiCall = () => (
    fetch(`${environment.ApiUrl}/employees/`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.token}`
        }
    }).then(response => response.json())
);

export const assetsHistoryByEmployeeRequestedApiCall = employeeName => (
    fetch(`${environment.ApiUrl}/records/`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.token}`
        }
    }).then(response => response.json())
);

export const assetsGetRequestedApiCall = () => (
    fetch(`${environment.ApiUrl}/assets/`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.token}`
        }
    }).then(response => response.json())
);

export const setAssetToEmployeeRequestedApiCall = (params) => (
    fetch(`${environment.ApiUrl}/assets/${params.assetId}`, {
        method: 'PATCH',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.token}`
        },
        body: JSON.stringify({
            employeeId: params.employeeId
        })
    }).then(response => response)
);