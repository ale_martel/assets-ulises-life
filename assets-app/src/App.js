import React, { Component } from 'react';
import './App.css';
import { IntlProvider } from 'react-intl';
import localization from './i18n/localization';
import PropTypes from 'prop-types';
import { authenticationRequested } from './actions';
import { connect } from  'react-redux';
import Routes from './routes';

class App extends Component {
  componentDidMount() {
    this.props.authenticate();
  }

  render(){
    const { lang } = this.props;
    return (
    <IntlProvider locale={lang} messages={localization[lang]}>
      <Routes/>
    </IntlProvider>
    )
  }
}

App.propTypes = {
  lang: PropTypes.string
};

const mapStateToProps = state => ({
  lang: state.locale.lang
});

const mapDispatchToProps = dispatch => ({
  authenticate: () => dispatch(authenticationRequested())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
