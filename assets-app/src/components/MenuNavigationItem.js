import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

const MenuNavigationItem = ( {route, textKey} ) => (
    <li className="nav-item">
        <NavLink to={route} className={"nav-link"} activeClassName={"active"}><FormattedMessage id={textKey}/></NavLink>
    </li>
);

MenuNavigationItem.propTypes = {
    route: PropTypes.string.isRequired,
    textKey: PropTypes.string.isRequired
};

export default MenuNavigationItem;