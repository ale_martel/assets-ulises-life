import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import EmployeesList from './EmployeesList'

Enzyme.configure({ adapter: new Adapter() })

function setup() {
  const props = {
      employees: [{id: 1, name: "aName"},{id: 2, name: "anotherName"}],
      selectedEmployee: {id: 1, name: "aName"},
      onClickFunc: jest.fn()
  }

  const enzymeWrapper = shallow(<EmployeesList {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('EmployeesList', () => {
    it('should render self and subcomponents', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('li').length).toEqual(2);
        expect(enzymeWrapper.find('li').get(0).props.className).toEqual('list-group-item active');
        expect(enzymeWrapper.find('li').get(1).props.className).toEqual('list-group-item ');
    })
})