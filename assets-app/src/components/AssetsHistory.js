import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedDate } from 'react-intl';

const AssetsHistory = ( {assets} ) => (
    <div className="card">
        <div className="card-header"><FormattedMessage id="ViewAssetsHistoryByEmployee.AssetsList.Header"/></div>
        <div className="card-body">
        <ul className="list-group">
            {
            assets.map(item => (
                <li className="list-group-item" key={item.id}>{item.asset} (<FormattedMessage id="ViewAssetsHistoryByEmployee.AssignedDate.Label"/>: <FormattedDate value={item.date} day="numeric" month="long" year="numeric"/>)</li>
            ))
            }
        </ul>
        </div>
    </div>
);

AssetsHistory.propTypes = {
    assets: PropTypes.array,
};

export default AssetsHistory;