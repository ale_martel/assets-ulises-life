import React from 'react';
import PropTypes from 'prop-types';

const FormInput = ( {name, onChangeFunc} ) => (
    <input type="text" className="form-control" name={name} onChange={onChangeFunc}/>
);

FormInput.propTypes = {
    name: PropTypes.string.isRequired,
    onChangeFunc: PropTypes.func.isRequired,
};

export default FormInput;