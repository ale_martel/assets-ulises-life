import React from 'react';
import PropTypes from 'prop-types';

const PrimaryButton = ( {text, onClickFunc} ) => (
    <input type="button" className="btn btn-primary" value={text} onClick={() => onClickFunc()} />
);

PrimaryButton.propTypes = {
    text: PropTypes.string.isRequired,
    onClickFunc: PropTypes.func.isRequired
};

export default PrimaryButton;