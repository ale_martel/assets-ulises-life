import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import AssetsHistory from './AssetsHistory'

Enzyme.configure({ adapter: new Adapter() })

function setup() {
  const props = {
      assets: [{
        "id": 1,
        "date": "2018-06-13T09:03:18.915Z",
        "fromEmployee": "Depot",
        "toEmployee": "Depot",
        "asset": "iPhone 5s"
    }],
  }

  const enzymeWrapper = shallow(<AssetsHistory {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('AssetsHistory', () => {
    it('should render self and subcomponents', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('li').length).toEqual(1);
        expect(enzymeWrapper.find('li').contains("iPhone 5s")).toBe(true);
    })
})