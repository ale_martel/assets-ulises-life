import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import MenuNavigationItem from './MenuNavigationItem'

Enzyme.configure({ adapter: new Adapter() })

function setup() {
  const props = {
      route: "employee/add",
      textKey: "key"
  }

  const enzymeWrapper = shallow(<MenuNavigationItem {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('MenuNavigationItem', () => {
    it('should render self and subcomponents', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('NavLink').hasClass('nav-link')).toBe(true);
        expect(enzymeWrapper.find('FormattedMessage').props().id).toBe("key");
    })
})