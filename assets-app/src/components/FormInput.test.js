import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import FormInput from './FormInput'

Enzyme.configure({ adapter: new Adapter() })

function setup() {
  const props = {
      name: "inputName",
      onChangeFunc: jest.fn()
  }

  const enzymeWrapper = shallow(<FormInput {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('FormInput', () => {
    it('should render self and subcomponents', () => {
        const { enzymeWrapper, props } = setup();

        expect(enzymeWrapper.find('input').hasClass('form-control')).toBe(true);
        expect(enzymeWrapper.find('input').props().name).toBe("inputName");
        expect(enzymeWrapper.find('input').props().onChange).toBe(props.onChangeFunc);
    })
})