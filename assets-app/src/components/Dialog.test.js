import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Dialog from './Dialog'

Enzyme.configure({ adapter: new Adapter() })

function setup() {
  const props = {
      text: "key",
  }

  const enzymeWrapper = shallow(<Dialog {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('Dialog', () => {
    it('should render self and subcomponents', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('div').length).toEqual(1);
        expect(enzymeWrapper.find('FormattedMessage').props().id).toEqual("key");
    })
})