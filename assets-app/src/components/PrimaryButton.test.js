import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import PrimaryButton from './PrimaryButton'

Enzyme.configure({ adapter: new Adapter() })

function setup() {
  const props = {
      text: "button text",
      onClickFunc: jest.fn()
  }

  const enzymeWrapper = shallow(<PrimaryButton {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('Primary button', () => {
    it('should render self and subcomponents', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('input').hasClass('btn-primary')).toBe(true);
        expect(enzymeWrapper.find('input').props().value).toBe("button text");
    })
})