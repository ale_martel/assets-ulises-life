import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import LanguageSelector from './LanguageSelector'

Enzyme.configure({ adapter: new Adapter() })

function setup() {
  const props = {
      languages: ["es", "en"],
      onClickFunc: jest.fn()
  }

  const enzymeWrapper = shallow(<LanguageSelector {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('LanguageSelector', () => {
    it('should render self and subcomponents', () => {
        const { enzymeWrapper } = setup();

        expect(enzymeWrapper.find('label').length).toEqual(2);
        expect(enzymeWrapper.find('label').get(0).props.className).toEqual('nav-link');
        expect(enzymeWrapper.find('label').get(1).props.className).toEqual('nav-link');
        expect(enzymeWrapper.find('label').get(0).props.children).toEqual("ES");
        expect(enzymeWrapper.find('label').get(1).props.children).toEqual("EN");
    })
})