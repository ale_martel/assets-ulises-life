import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

const EmployeesList = ( {employees, selectedEmployee, onClickFunc} ) => (
    <div className="card">
        <div className="card-header"><FormattedMessage id="ViewAssetsHistoryByEmployee.EmployeeList.Header"/>:</div>
        <div className="card-body">
        <ul className="list-group">
            {
            employees.map(employee => (
                <li className={"list-group-item " + (selectedEmployee != null && employee.id === selectedEmployee.id ? 'active' : '')} key={employee.id}>
                    <label onClick={() => onClickFunc(employee)}>{employee.name}</label>
                </li>
            ))
            }
        </ul>
        </div>
    </div>

);

EmployeesList.propTypes = {
    employees: PropTypes.array,
    selectedEmployee: PropTypes.object,
    onClickFunc: PropTypes.func.isRequired
};

export default EmployeesList;