import React from 'react';
import PropTypes from 'prop-types';

const LanguageSelector = ( {languages, onClickFunc} ) => (
    <div className="form-inline my-2 my-lg-0">
        {languages.map(lang => (
            <label key={lang} className="nav-link" onClick={() => onClickFunc(lang)}>{lang.toUpperCase()}</label>
        ))}
    </div>
);

LanguageSelector.propTypes = {
    languagues: PropTypes.array
};

export default LanguageSelector;