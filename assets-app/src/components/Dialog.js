import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

let dialogStyles = {
    width: "500px",
    maxWidth: "100%",
    margin: "0 auto",
    position: "fixed",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
    zIndex: "999",
    backgroundColor: "#eee",
    padding: "10px 20px 40px",
    borderRadius: "8px",
    display: "flex",
    flexDirection: "column"
} 
let dialogCloseButtonStyles = {
    marginBottom: "15px",
    padding: "3px 8px",
    cursor: "pointer",
    borderRadius: "50px",
    border: "none",
    width: "30px",
    height: "30px",
    fontWeight: "bold",
    alignSelf: "flex-end"
}

const Dialog = ( {text} ) => (
    <div style={dialogStyles}>
        <button style={dialogCloseButtonStyles} onClick={() => window.location.reload()}>x</button>
        <FormattedMessage id={text}/>
    </div>
);

Dialog.propTypes = {
    text: PropTypes.string
};

export default Dialog;