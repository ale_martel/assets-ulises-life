import * as types from './actionTypes';

const employeeAddRequested = (employeeName) => ({
  type: types.EMPLOYEE_ADD_REQUESTED,
  payload: {employeeName},
});

export default employeeAddRequested;