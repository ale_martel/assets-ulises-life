import * as types from './actionTypes';

const assetsGetFailed = ({ message }) => ({
  type: types.ASSETS_GET_FAILED,
  payload: { message },
});

export default assetsGetFailed;