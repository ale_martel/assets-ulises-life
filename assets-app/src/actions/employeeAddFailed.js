import * as types from './actionTypes';

const employeeAddFailed = ({ message }) => ({
  type: types.EMPLOYEE_ADD_FAILED,
  payload: { message },
});

export default employeeAddFailed;