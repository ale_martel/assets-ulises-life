import * as types from './actionTypes';

const assetsHistoryByEmployeeFailed = ({ message }) => ({
  type: types.ASSETS_HISTORY_BY_EMPLOYEE_FAILED,
  payload: { message },
});

export default assetsHistoryByEmployeeFailed;