import * as types from './actionTypes';

const assetSelectedSucceeded = (assetId) => ({
  type: types.ASSET_SELECTED,
  payload: assetId,
});

export default assetSelectedSucceeded;