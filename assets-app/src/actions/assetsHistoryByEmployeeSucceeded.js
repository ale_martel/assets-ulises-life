import * as types from './actionTypes';

const assetsHistoryByEmployeeSucceeded = (assetsHistoryByEmployee) => ({
  type: types.ASSETS_HISTORY_BY_EMPLOYEE_SUCCEEDED,
  payload: assetsHistoryByEmployee,
});

export default assetsHistoryByEmployeeSucceeded;