import { LOCALE_SET } from './actionTypes';

const setLocale = (lang) => ({
    type: LOCALE_SET,
    lang,
  });
  
  export default setLocale;