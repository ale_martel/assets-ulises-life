import * as types from './actionTypes';

const assetAddRequested = (assetName, assetNotes, assetIsBlocked, assetSerialNumber) => ({
  type: types.ASSET_ADD_REQUESTED,
  payload: { assetName, assetNotes, assetIsBlocked, assetSerialNumber },
});

export default assetAddRequested;