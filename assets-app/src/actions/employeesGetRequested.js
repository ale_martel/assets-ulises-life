import * as types from './actionTypes';

const employeesGetRequested = () => ({
  type: types.EMPLOYEES_GET_REQUESTED,
  payload: {}
});

export default employeesGetRequested;