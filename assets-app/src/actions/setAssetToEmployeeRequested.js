import * as types from './actionTypes';

const setAssetToEmployeeRequested = (assetId, employeeId) => ({
  type: types.SET_ASSET_TO_EMPLOYEE_REQUESTED,
  payload: { assetId, employeeId },
});

export default setAssetToEmployeeRequested;