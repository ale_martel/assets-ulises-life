import * as types from './actionTypes';

const assetAddSucceeded = (asset) => ({
  type: types.ASSET_ADD_SUCCEEDED,
  payload: { asset },
});

export default assetAddSucceeded;