import * as types from './actionTypes';

const employeesGetSucceeded = (employees) => ({
  type: types.EMPLOYEES_GET_SUCCEEDED,
  payload: employees,
});

export default employeesGetSucceeded;