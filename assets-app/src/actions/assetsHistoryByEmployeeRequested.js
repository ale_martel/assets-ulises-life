import * as types from './actionTypes';

const assetsHistoryByEmployeeRequested = (employee) => ({
  type: types.ASSETS_HISTORY_BY_EMPLOYEE_REQUESTED,
  payload: employee
});

export default assetsHistoryByEmployeeRequested;