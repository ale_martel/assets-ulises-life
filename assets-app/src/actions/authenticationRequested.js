import * as types from './actionTypes';

const auhtenticationRequested = () => ({
  type: types.AUTHENTICATION_REQUESTED,
  payload: {},
});

export default auhtenticationRequested;