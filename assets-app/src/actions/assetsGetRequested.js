import * as types from './actionTypes';

const assetsGetRequested = () => ({
  type: types.ASSETS_GET_REQUESTED,
  payload: {},
});

export default assetsGetRequested;