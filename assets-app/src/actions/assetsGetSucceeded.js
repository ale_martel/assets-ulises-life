import * as types from './actionTypes';

const assetsGetSucceeded = (assets) => ({
  type: types.ASSETS_GET_SUCCEEDED,
  payload: assets,
});

export default assetsGetSucceeded;