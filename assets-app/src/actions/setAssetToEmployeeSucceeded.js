import * as types from './actionTypes';

const setAssetToEmployeeSucceeded = () => ({
  type: types.SET_ASSET_TO_EMPLOYEE_SUCCEEDED,
  payload: {},
});

export default setAssetToEmployeeSucceeded;