import * as types from './actionTypes';

const setAssetToEmployeeFailed = ({ message }) => ({
  type: types.SET_ASSET_TO_EMPLOYEE_FAILED,
  payload: { message },
});

export default setAssetToEmployeeFailed;