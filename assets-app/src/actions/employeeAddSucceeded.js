import * as types from './actionTypes';

const employeeAddSucceeded = (employee) => ({
  type: types.EMPLOYEE_ADD_SUCCEEDED,
  payload: { employee },
});

export default employeeAddSucceeded;