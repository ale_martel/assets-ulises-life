import * as types from './actionTypes';

const employeesGetFailed = ({ message }) => ({
  type: types.EMPLOYEES_GET_FAILED,
  payload: { message },
});

export default employeesGetFailed;