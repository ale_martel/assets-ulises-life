import * as types from './actionTypes';

const authenticationFailed = ({ message }) => ({
  type: types.AUTHENTICATION_FAILED,
  payload: { message },
});

export default authenticationFailed;