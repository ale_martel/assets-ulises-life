import * as types from './actionTypes';

const assetAddFailed = ({ message }) => ({
  type: types.ASSET_ADD_FAILED,
  payload: { message },
});

export default assetAddFailed;