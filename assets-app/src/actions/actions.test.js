import authenticationRequested from './authenticationRequested';
import authenticationSucceeded from './authenticationSucceeded';
import authenticationFailed from './authenticationFailed';
import employeeAddRequested from './employeeAddRequested';
import employeeAddFailed from './employeeAddFailed';
import employeeAddSucceeded from './employeeAddSucceeded';
import assetAddRequested from './assetAddRequested';
import assetAddFailed from './assetAddFailed';
import assetAddSucceeded from './assetAddSucceeded';
import employeesGetRequested from './employeesGetRequested';
import employeesGetFailed from './employeesGetFailed';
import employeesGetSucceeded from './employeesGetSucceeded';
import assetsHistoryByEmployeeRequested from './assetsHistoryByEmployeeRequested';
import assetsHistoryByEmployeeFailed from './assetsHistoryByEmployeeFailed';
import assetsHistoryByEmployeeSucceeded from './assetsHistoryByEmployeeSucceeded';
import assetsGetRequested from './assetsGetRequested';
import assetsGetFailed from './assetsGetFailed';
import assetsGetSucceeded from './assetsGetSucceeded';
import assetSelectedSucceeded from './assetSelectedSucceeded';
import setAssetToEmployeeRequested from './setAssetToEmployeeRequested';
import setAssetToEmployeeFailed from './setAssetToEmployeeFailed';
import setAssetToEmployeeSucceeded from './setAssetToEmployeeSucceeded';
import * as types from './actionTypes'

describe('actions', () => {
  it('should create an action to authenticate', () => {
    const payload = {};
    const expectedAction = {
      type: types.AUTHENTICATION_REQUESTED,
      payload
    }
    expect(authenticationRequested(payload)).toEqual(expectedAction);
  });
  it('should create an action to hanlde success when authenticating', () => {
    const token = "aToken";
    const expectedAction = {
      type: types.AUTHENTICATION_SUCCEEDED,
      payload: {token}
    }
    expect(authenticationSucceeded(token)).toEqual(expectedAction);
  });
  it('should create an action to hanlde failure when authenticating', () => {
    const payload = {message: "an message"};
    const expectedAction = {
      type: types.AUTHENTICATION_FAILED,
      payload
    }
    expect(authenticationFailed(payload)).toEqual(expectedAction);
  });

  it('should create an action to add an employee', () => {
    const payload = {employeeName: "aName"};
    const expectedAction = {
      type: types.EMPLOYEE_ADD_REQUESTED,
      payload
    }
    expect(employeeAddRequested(payload.employeeName)).toEqual(expectedAction);
  });
  it('should create an action to hanlde success when adding an employee', () => {
    const employee = {name: "employee name"};
    const expectedAction = {
      type: types.EMPLOYEE_ADD_SUCCEEDED,
      payload: {employee}
    }
    expect(employeeAddSucceeded(employee)).toEqual(expectedAction);
  });
  it('should create an action to hanlde failure when adding an employee', () => {
    const payload = {message: "an message"};
    const expectedAction = {
      type: types.EMPLOYEE_ADD_FAILED,
      payload
    }
    expect(employeeAddFailed(payload)).toEqual(expectedAction);
  });

  it('should create an action to add an asset', () => {
    const payload = {assetName: "aName", assetNotes: "someNotes", assetIsBlocked: true, assetSerialNumber: "aSerialNumber"};
    const expectedAction = {
      type: types.ASSET_ADD_REQUESTED,
      payload
    }
    expect(assetAddRequested(payload.assetName, payload.assetNotes, payload.assetIsBlocked, payload.assetSerialNumber)).toEqual(expectedAction);
  });
  it('should create an action to hanlde success when adding an asset', () => {
    const asset = {name: "aName", notes: "someNotes", blocked: true, serialNumber: "aSerialNumber"};
    const expectedAction = {
      type: types.ASSET_ADD_SUCCEEDED,
      payload: {asset}
    }
    expect(assetAddSucceeded(asset)).toEqual(expectedAction);
  });
  it('should create an action to hanlde failure when adding an asset', () => {
    const payload = {message: "an message"};
    const expectedAction = {
      type: types.ASSET_ADD_FAILED,
      payload
    }
    expect(assetAddFailed(payload)).toEqual(expectedAction);
  });

  it('should create an action to get employees', () => {
    const payload = {};
    const expectedAction = {
      type: types.EMPLOYEES_GET_REQUESTED,
      payload
    }
    expect(employeesGetRequested()).toEqual(expectedAction);
  });
  it('should create an action to hanlde success when getting employees', () => {
    const employees = [{
        "id": 1,
        "createDate": "2018-06-13T08:50:00.592Z",
        "updateDate": "2018-06-13T08:50:00.592Z",
        "name": "Depot"
    }];
    const expectedAction = {
      type: types.EMPLOYEES_GET_SUCCEEDED,
      payload: employees
    }
    expect(employeesGetSucceeded(employees)).toEqual(expectedAction);
  });
  it('should create an action to hanlde failure when getting employees', () => {
    const payload = {message: "an message"};
    const expectedAction = {
      type: types.EMPLOYEES_GET_FAILED,
      payload
    }
    expect(employeesGetFailed(payload)).toEqual(expectedAction);
  });

  it('should create an action to get assets history by employee', () => {
    const employeeName = "aName";
    const expectedAction = {
      type: types.ASSETS_HISTORY_BY_EMPLOYEE_REQUESTED,
      payload: employeeName
    }
    expect(assetsHistoryByEmployeeRequested(employeeName)).toEqual(expectedAction);
  });
  it('should create an action to hanlde success when getting assets history by employee', () => {
    const assetsHistoryByEmployee = [{
      id: 1,
      date: "2018-06-13T09:03:18.915Z",
      fromEmployee: "Depot",
      toEmployee: "Depot",
      asset: "iPhone 5s"
    }];
    const expectedAction = {
      type: types.ASSETS_HISTORY_BY_EMPLOYEE_SUCCEEDED,
      payload: assetsHistoryByEmployee
    }
    expect(assetsHistoryByEmployeeSucceeded(assetsHistoryByEmployee)).toEqual(expectedAction);
  });
  it('should create an action to hanlde failure when getting assets history by employee', () => {
    const payload = {message: "an message"};
    const expectedAction = {
      type: types.ASSETS_HISTORY_BY_EMPLOYEE_FAILED,
      payload
    }
    expect(assetsHistoryByEmployeeFailed(payload)).toEqual(expectedAction);
  });

  it('should create an action to get assets', () => {
    const payload = {};
    const expectedAction = {
      type: types.ASSETS_GET_REQUESTED,
      payload
    }
    expect(assetsGetRequested()).toEqual(expectedAction);
  });
  it('should create an action to hanlde success when getting assets', () => {
    const assets = [{
      id: 1,
      createDate: "2018-06-13T09:03:18.908Z",
      updateDate: "2018-06-13T09:03:18.908Z",
      name: "iPhone 5s",
      notes: null,
      blocked: false,
      serialNumber: "07fa0g9a70fq09u0bq90ba",
      employeeId: 2
    }];
    const expectedAction = {
      type: types.ASSETS_GET_SUCCEEDED,
      payload: assets
    }
    expect(assetsGetSucceeded(assets)).toEqual(expectedAction);
  });
  it('should create an action to hanlde failure when getting assets', () => {
    const payload = {message: "an message"};
    const expectedAction = {
      type: types.ASSETS_GET_FAILED,
      payload
    }
    expect(assetsGetFailed(payload)).toEqual(expectedAction);
  });

  it('should create an action to select and asset', () => {
    const assetId = 2;
    const expectedAction = {
      type: types.ASSET_SELECTED,
      payload: assetId
    }
    expect(assetSelectedSucceeded(assetId)).toEqual(expectedAction);
  });

  it('should create an action to set asset to employee', () => {
    const payload = {};
    const expectedAction = {
      type: types.SET_ASSET_TO_EMPLOYEE_REQUESTED,
      payload
    }
    expect(setAssetToEmployeeRequested()).toEqual(expectedAction);
  });
  it('should create an action to hanlde success when setting asset to employee', () => {
    const expectedAction = {
      type: types.SET_ASSET_TO_EMPLOYEE_SUCCEEDED,
      payload: {}
    }
    expect(setAssetToEmployeeSucceeded()).toEqual(expectedAction);
  });
  it('should create an action to hanlde failure setting asset to employee', () => {
    const payload = {message: "an message"};
    const expectedAction = {
      type: types.SET_ASSET_TO_EMPLOYEE_FAILED,
      payload
    }
    expect(setAssetToEmployeeFailed(payload)).toEqual(expectedAction);
  });
});