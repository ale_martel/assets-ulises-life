import * as types from './actionTypes';

const authenticationSucceeded = (token) => ({
  type: types.AUTHENTICATION_SUCCEEDED,
  payload: { token },
});

export default authenticationSucceeded;