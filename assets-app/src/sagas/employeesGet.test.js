var assert = require('assert');
import employeesGet from './employeesGet';
import { call, put } from 'redux-saga/effects';
import * as apiCalls from '../restApi';
import { employeesGetFailed, employeesGetSucceeded } from '../actions';

describe('employees get should', () => {
    test('call backend to get employees', () => {

        const generator = employeesGet();
                
        assert.deepEqual(generator.next().value, call(apiCalls.employeesRequestedApiCall));
    });
    
    test('handle error when call to backend fails', () => {

        const generator = employeesGet();
        
        assert.deepEqual(generator.next().value, call(apiCalls.employeesRequestedApiCall));
        assert.deepEqual(generator.throw('message').value, put(employeesGetFailed('message')));
    });

    test('handle success when call to backend finish ok', () => {

        const generator = employeesGet();
        
        var expectedEmployees = [{
            id: 1,
            name: "aName"
        }];
        assert.deepEqual(generator.next().value, call(apiCalls.employeesRequestedApiCall));
        assert.deepEqual(generator.next(expectedEmployees).value, put(employeesGetSucceeded(expectedEmployees)));
    });
});
