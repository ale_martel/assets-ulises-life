import { call, put } from 'redux-saga/effects';
import { authenticationSucceeded, authenticationFailed } from '../actions';
import * as apiCalls from '../restApi';

const authenticationRequested = function* authenticationRequested() {
  try {
    const tokenResponse = yield call(apiCalls.authenticationRequestedApiCall);
    yield put(authenticationSucceeded(tokenResponse.token));
  } catch (e) {
    yield put(authenticationFailed({e}));
  }
};

export default authenticationRequested;