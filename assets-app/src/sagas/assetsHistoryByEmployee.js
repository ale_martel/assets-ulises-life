import { call, put } from 'redux-saga/effects';
import { assetsHistoryByEmployeeFailed, assetsHistoryByEmployeeSucceeded } from '../actions';
import * as apiCalls from '../restApi';

const assetsHistoryByEmployeeRequested = function* assetsHistoryByEmployeeRequested(action) {
  try {
    const employee = action.payload;
    const assetsHistory = yield call(apiCalls.assetsHistoryByEmployeeRequestedApiCall, employee.name);
    yield put(assetsHistoryByEmployeeSucceeded(assetsHistory.filter(records => records.toEmployee === employee.name)));
  } catch (e) {
    yield put(assetsHistoryByEmployeeFailed({ e }));
  }
};

export default assetsHistoryByEmployeeRequested;