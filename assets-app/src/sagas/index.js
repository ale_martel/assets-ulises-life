import { takeLatest, all } from 'redux-saga/effects';
import * as types from '../actions/actionTypes';
import authenticationAddSaga from './authentication';
import employeeAddSaga from './employeeAdd';
import assetAddSaga from './assetAdd';
import employeesGetSaga from './employeesGet';
import assetsHistoryByEmployeeSaga from './assetsHistoryByEmployee';
import assetsGetSaga from './assetsGet';
import setAssetToEmployeeSaga from './setAssetToEmployee';

export default function* rootSaga() {
    yield all([employeeAddRequestedSaga(),
               assetAddRequestedSaga(),
               authenticationRequestedSaga(),
               employeesGetRequestedSaga(),
               assetsHistoryByEmployeeRequestedSaga(),
               assetsGetRequestedSaga(),
               setAssetToEmployeeRequestedSaga()])
}

const employeeAddRequestedSaga = function* employeeAddRequestedSaga() {
    yield takeLatest(types.EMPLOYEE_ADD_REQUESTED, employeeAddSaga);
};

const assetAddRequestedSaga = function* assetAddRequestedSaga() {
    yield takeLatest(types.ASSET_ADD_REQUESTED, assetAddSaga);
};

const authenticationRequestedSaga = function* authenticationRequestedSaga() {
    yield takeLatest(types.AUTHENTICATION_REQUESTED, authenticationAddSaga);
};

const employeesGetRequestedSaga = function* employeesGetRequestedSaga() {
    yield takeLatest(types.EMPLOYEES_GET_REQUESTED, employeesGetSaga);
};

const assetsHistoryByEmployeeRequestedSaga = function* assetsHistoryByEmployeeRequestedSaga() {
    yield takeLatest(types.ASSETS_HISTORY_BY_EMPLOYEE_REQUESTED, assetsHistoryByEmployeeSaga);
};

const assetsGetRequestedSaga = function* assetsGetRequestedSaga() {
    yield takeLatest(types.ASSETS_GET_REQUESTED, assetsGetSaga);
};

const setAssetToEmployeeRequestedSaga = function* setAssetToEmployeeRequestedSaga() {
    yield takeLatest(types.SET_ASSET_TO_EMPLOYEE_REQUESTED, setAssetToEmployeeSaga);
};