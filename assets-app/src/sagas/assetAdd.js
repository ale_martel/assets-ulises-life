import { call, put } from 'redux-saga/effects';
import { assetAddFailed, assetAddSucceeded } from '../actions';
import * as apiCalls from '../restApi';

const assetAddRequested = function* assetAddRequested(action) {
  const { assetName, assetNotes, assetIsBlocked, assetSerialNumber } = action.payload;
  try {
    const asset = yield call(apiCalls.assetAddRequestedApiCall, 
                            { name: assetName, 
                              notes: assetNotes, 
                              isBlocked: assetIsBlocked, 
                              serialNumber: assetSerialNumber 
                            });
    yield put(assetAddSucceeded(asset));
  } catch (e) {
    yield put(assetAddFailed({ e }));
  }
};

export default assetAddRequested;