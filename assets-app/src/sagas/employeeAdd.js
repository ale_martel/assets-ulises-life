import { call, put } from 'redux-saga/effects';
import { employeeAddFailed, employeeAddSucceeded } from '../actions';
import * as apiCalls from '../restApi';

const employeeAddRequested = function* employeeAddRequested(action) {
  const { employeeName } = action.payload;
  try {
    const employee = yield call(apiCalls.employeeAddRequestedApiCall, employeeName);
    yield put(employeeAddSucceeded(employee));
  } catch (e) {
    yield put(employeeAddFailed({e}));
  }
};

export default employeeAddRequested;