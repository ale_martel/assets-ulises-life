var assert = require('assert');
import assetAdd from './assetAdd';
import { call, put } from 'redux-saga/effects';
import * as apiCalls from '../restApi';
import { assetAddFailed, assetAddSucceeded } from '../actions';

describe('asset add should', () => {
    test('call backend to add asset', () => {
        const assetName = "aName";
        const assetNotes = "someNotes";
        const assetIsBlocked = true;
        const assetSerialNumber = "aSerialNumber";

        const generator = assetAdd({payload: {assetName, assetNotes, assetIsBlocked, assetSerialNumber}});
                
        assert.deepEqual(generator.next().value, call(apiCalls.assetAddRequestedApiCall, {   
            name: assetName, 
            notes: assetNotes, 
            isBlocked: assetIsBlocked, 
            serialNumber: assetSerialNumber 
        }));
    });
    
    test('handle error when call to backend fails', () => {
        const assetName = "aName";
        const assetNotes = "someNotes";
        const assetIsBlocked = true;
        const assetSerialNumber = "aSerialNumber";

        const generator = assetAdd({payload: {assetName, assetNotes, assetIsBlocked, assetSerialNumber}});
        
        assert.deepEqual(generator.next().value, call(apiCalls.assetAddRequestedApiCall, {
            name: assetName, 
            notes: assetNotes, 
            isBlocked: assetIsBlocked, 
            serialNumber: assetSerialNumber 
        }));
        assert.deepEqual(generator.throw('message').value, put(assetAddFailed('message')));
    });

    test('handle success when call to backend finish ok', () => {
        const assetName = "aName";
        const assetNotes = "someNotes";
        const assetIsBlocked = true;
        const assetSerialNumber = "aSerialNumber";

        const generator = assetAdd({payload: {assetName, assetNotes, assetIsBlocked, assetSerialNumber}});
        
        var expectedAsset = {
            name: assetName, 
            notes: assetNotes, 
            isBlocked: assetIsBlocked, 
            serialNumber: assetSerialNumber 
        };
        assert.deepEqual(generator.next().value, call(apiCalls.assetAddRequestedApiCall, {
            name: assetName, 
            notes: assetNotes, 
            isBlocked: assetIsBlocked, 
            serialNumber: assetSerialNumber 
        }));
        assert.deepEqual(generator.next(expectedAsset).value, put(assetAddSucceeded({
            name: assetName, 
            notes: assetNotes, 
            isBlocked: assetIsBlocked, 
            serialNumber: assetSerialNumber 
        })));
    });
});
