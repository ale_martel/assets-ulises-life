var assert = require('assert');
import setAssetToEmployee from './setAssetToEmployee';
import { call, put } from 'redux-saga/effects';
import * as apiCalls from '../restApi';
import { setAssetToEmployeeFailed, setAssetToEmployeeSucceeded } from '../actions';

describe('set asset to employee should', () => {
    test('call backend to set asset to employee', () => {
        const assetId = 2;
        const employeeId = 3;

        const generator = setAssetToEmployee({payload: {assetId, employeeId}});
                
        assert.deepEqual(generator.next().value, call(apiCalls.setAssetToEmployeeRequestedApiCall, {   
            assetId, 
            employeeId
        }));
    });
    
    test('handle error when call to backend fails', () => {
        const assetId = 2;
        const employeeId = 3;

        const generator = setAssetToEmployee({payload: {assetId, employeeId}});
        
        assert.deepEqual(generator.next().value, call(apiCalls.setAssetToEmployeeRequestedApiCall, {
            assetId, 
            employeeId
        }));
        assert.deepEqual(generator.throw('message').value, put(setAssetToEmployeeFailed('message')));
    });

    test('handle success when call to backend finish ok', () => {
        const assetId = 2;
        const employeeId = 3;

        const generator = setAssetToEmployee({payload: {assetId, employeeId}});
        
        var expectedAsset = {};
        assert.deepEqual(generator.next().value, call(apiCalls.setAssetToEmployeeRequestedApiCall, {
            assetId, 
            employeeId
        }));
        assert.deepEqual(generator.next(expectedAsset).value, put(setAssetToEmployeeSucceeded()));
    });
});
