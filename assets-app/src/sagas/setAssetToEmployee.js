import { call, put } from 'redux-saga/effects';
import { setAssetToEmployeeFailed, setAssetToEmployeeSucceeded } from '../actions';
import * as apiCalls from '../restApi';

const setAssetToEmployee = function* setAssetToEmployee(action) {
  const { assetId, employeeId } = action.payload;
  console.log(action.payload);
  try {
    yield call(apiCalls.setAssetToEmployeeRequestedApiCall, 
                            { assetId, 
                              employeeId
                            });
    yield put(setAssetToEmployeeSucceeded());
  } catch (e) {
    console.log(e);
    yield put(setAssetToEmployeeFailed({ e }));
  }
};

export default setAssetToEmployee;