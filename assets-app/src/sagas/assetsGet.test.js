var assert = require('assert');
import assetsGet from './assetsGet';
import { call, put } from 'redux-saga/effects';
import * as apiCalls from '../restApi';
import { assetsGetFailed, assetsGetSucceeded } from '../actions';

describe('assets get should', () => {
    test('call backend to get assets', () => {

        const generator = assetsGet();
                
        assert.deepEqual(generator.next().value, call(apiCalls.assetsGetRequestedApiCall));
    });
    
    test('handle error when call to backend fails', () => {

        const generator = assetsGet();
        
        assert.deepEqual(generator.next().value, call(apiCalls.assetsGetRequestedApiCall));
        assert.deepEqual(generator.throw('message').value, put(assetsGetFailed('message')));
    });

    test('handle success when call to backend finish ok', () => {

        const generator = assetsGet();
        
        var expectedAssets = [{
            id: 1,
            createDate: "2018-06-13T09:03:18.908Z",
            updateDate: "2018-06-13T09:03:18.908Z",
            name: "iPhone 5s",
            notes: null,
            blocked: false,
            serialNumber: "07fa0g9a70fq09u0bq90ba",
            employeeId: 2
        }];
        assert.deepEqual(generator.next().value, call(apiCalls.assetsGetRequestedApiCall));
        assert.deepEqual(generator.next(expectedAssets).value, put(assetsGetSucceeded(expectedAssets)));
    });
});
