var assert = require('assert');
import authentication from './authentication';
import { call, put } from 'redux-saga/effects';
import * as apiCalls from '../restApi';
import { authenticationFailed, authenticationSucceeded } from '../actions';

describe('authentication should', () => {
    test('call backend to authenticate', () => {

        const generator = authentication();
                
        assert.deepEqual(generator.next().value, call(apiCalls.authenticationRequestedApiCall));
    });

    test('handle error when call to backend fails', () => {

        const generator = authentication();
        
        assert.deepEqual(generator.next().value, call(apiCalls.authenticationRequestedApiCall));
        assert.deepEqual(generator.throw('message').value, put(authenticationFailed('message')));
    });

    test('handle success when call to backend finish ok', () => {

        const generator = authentication();
        
        var expectedTokenResponse = {token: "token"};
        assert.deepEqual(generator.next().value, call(apiCalls.authenticationRequestedApiCall));
        assert.deepEqual(generator.next(expectedTokenResponse).value, put(authenticationSucceeded(expectedTokenResponse.token)));
    });
});