var assert = require('assert');
import assetsHistoryByEmployee from './assetsHistoryByEmployee';
import { call, put } from 'redux-saga/effects';
import * as apiCalls from '../restApi';
import { assetsHistoryByEmployeeFailed, assetsHistoryByEmployeeSucceeded } from '../actions';

describe('assets history by employee should', () => {
    test('call backend to get assets history by employee', () => {
        const employee = {name: "employeeName"};

        const generator = assetsHistoryByEmployee({payload: employee});
                
        assert.deepEqual(generator.next().value, call(apiCalls.assetsHistoryByEmployeeRequestedApiCall, employee.name));
    });
    
    test('handle error when call to backend fails', () => {
        const employee = {name: "employeeName"};

        const generator = assetsHistoryByEmployee({payload: employee});
        
        assert.deepEqual(generator.next().value, call(apiCalls.assetsHistoryByEmployeeRequestedApiCall, employee.name));
        assert.deepEqual(generator.throw('message').value, put(assetsHistoryByEmployeeFailed('message')));
    });

    test('handle success when call to backend finish ok', () => {
        const employee = {name: "employeeName"};

        const generator = assetsHistoryByEmployee({payload: employee});
        
        var expectedAssetsHistory = [{
            asset: "iPhone 5s",
            toEmployee: employee.name
        }];
        assert.deepEqual(generator.next().value, call(apiCalls.assetsHistoryByEmployeeRequestedApiCall, employee.name));
        assert.deepEqual(generator.next(expectedAssetsHistory).value, put(assetsHistoryByEmployeeSucceeded(expectedAssetsHistory.filter(records => records.toEmployee == employee.name))));
    });
});
