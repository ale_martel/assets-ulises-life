import { call, put } from 'redux-saga/effects';
import { employeesGetFailed, employeesGetSucceeded } from '../actions';
import * as apiCalls from '../restApi';

const employeesGetRequested = function* employeesGetRequested() {
  try {
    const assetsHistory = yield call(apiCalls.employeesRequestedApiCall);
    yield put(employeesGetSucceeded(assetsHistory));
  } catch (e) {
    yield put(employeesGetFailed({ e }));
  }
};

export default employeesGetRequested;