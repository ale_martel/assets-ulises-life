import { call, put } from 'redux-saga/effects';
import { assetsGetFailed, assetsGetSucceeded } from '../actions';
import * as apiCalls from '../restApi';

const assetsGetRequested = function* assetsGetRequested() {
  try {
    const assets = yield call(apiCalls.assetsGetRequestedApiCall);
    yield put(assetsGetSucceeded(assets));
  } catch (e) {
    yield put(assetsGetFailed({ e }));
  }
};

export default assetsGetRequested;