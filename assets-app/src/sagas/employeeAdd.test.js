var assert = require('assert');
import employeeAdd from './employeeAdd';
import { call, put } from 'redux-saga/effects';
import * as apiCalls from '../restApi';
import { employeeAddFailed, employeeAddSucceeded } from '../actions';

describe('employee add should', () => {
    test('call backend to add employee', () => {
        const employeeName = "employeeName";

        const generator = employeeAdd({payload: {employeeName}});
                
        assert.deepEqual(generator.next().value, call(apiCalls.employeeAddRequestedApiCall, employeeName));
    });

    test('handle error when call to backend fails', () => {
        const employeeName = "employeeName";

        const generator = employeeAdd({payload: {employeeName}});
        
        assert.deepEqual(generator.next().value, call(apiCalls.employeeAddRequestedApiCall, employeeName));
        assert.deepEqual(generator.throw('message').value, put(employeeAddFailed('message')));
    });

    test('handle success when call to backend finish ok', () => {
        const employeeName = "employeeName";

        const generator = employeeAdd({payload: {employeeName}});
        
        var expectedEmployee = {name: employeeName};
        assert.deepEqual(generator.next().value, call(apiCalls.employeeAddRequestedApiCall, employeeName));
        assert.deepEqual(generator.next(expectedEmployee).value, put(employeeAddSucceeded(expectedEmployee)));
    });
});